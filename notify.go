package notify

import "context"

// Notifier
type Notifier interface {
	Notify(ctx context.Context) error
	Type() string
}

// 重试
func Retry(notice Notifier, count int) error {
	var err error
	for i := 0; i < count; i++ {
		err = notice.Notify(context.Background())
		if err == nil {
			return nil
		}
	}
	return err
}
