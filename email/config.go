package email

import (
	"strconv"
)

type Secret string

// EmailServer 邮箱服务器
type EmailServer struct {
	Host string
	Port int
	Ssl  bool
}

// String return host:port
func (s *EmailServer) String() string {
	return s.Host + ":" + strconv.Itoa(s.Port)
}

var (
	// TencentEnterpriseSMTPServer 腾讯企业邮箱服务器
	TencentEnterpriseSMTPServer = EmailServer{Host: "smtp.exmail.qq.com", Port: 465, Ssl: true}
)

// EmailConfig configures notifications via mail.
type EmailConfig struct {
	From         string                 `yaml:"from,omitempty" json:"from,omitempty"`                   // 发件人
	To           string                 `yaml:"to,omitempty" json:"to,omitempty"`                       // 收件人
	Cc           string                 `yaml:"cc,omitempty" json:"cc,omitempty"`                       // 抄送人
	Bcc          string                 `yaml:"bcc,omitempty" json:"bcc,omitempty"`                     // 密送人
	EmailServer  EmailServer            `yaml:"email_server,omitempty" json:"email_server,omitempty"`   // 邮箱服务器
	AuthUsername string                 `yaml:"auth_username,omitempty" json:"auth_username,omitempty"` // 邮箱账号
	AuthPassword Secret                 `yaml:"auth_password,omitempty" json:"auth_password,omitempty"` // 邮箱密码
	AuthSecret   Secret                 `yaml:"auth_secret,omitempty" json:"auth_secret,omitempty"`     // 邮箱
	Subject      string                 `yaml:"subject,omitempty" json:"subject"`                       // 主题
	Template     string                 `yaml:"tempalte,omitempty" json:"tempalte,omitempty"`           // 模板字符串
	TemplateFile string                 `yaml:"template_file,omitempty" json:"template_file,omitempty"` // 模板文件
	Data         map[string]interface{} `yaml:"data" json:"data"`                                       // 内容
}
