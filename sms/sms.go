package sms

import (
	"context"
)

type Sms struct {
	conf *SmsConfig
}

func New(c *SmsConfig) *Sms {
	return &Sms{conf: c}
}

func (s *Sms) Notify(ctx context.Context) error {
	return nil
}
